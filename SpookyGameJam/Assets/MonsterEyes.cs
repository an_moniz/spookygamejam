﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterEyes : MonoBehaviour
{
    public enum State
    {
        Awake,
        Sleepy,
        Asleep
    }

    [SerializeField] private Sprite _awakeEyes;
    [SerializeField] private Sprite _sleepyEyes;

    private SpriteRenderer _eyes;

    public bool flipX
    {
        get { return _eyes.flipX; }
        set { _eyes.flipX = value; }
    }
    public bool flipY
    {
        get { return _eyes.flipY; }
        set { _eyes.flipY = value; }
    }

    void Awake()
    {
        _eyes = GetComponent<SpriteRenderer>();    
    }

    public void SetState( State state )
    {
        _eyes.sprite = (state == State.Awake) ? _awakeEyes : _sleepyEyes;
        _eyes.color = (state == State.Asleep) ? Color.black : Color.white;
    }
}
