﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TransitionScreen : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _text;

    public void SetText( string text )
    {
        _text.SetText(text);
    }
}
