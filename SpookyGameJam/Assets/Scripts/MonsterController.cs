using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : MovementController
{
    [SerializeField]
    private float _wakeTime = 1.5f;
    [SerializeField]
    private int _alertDelay = 2;
    [SerializeField]
    private SpriteRenderer _renderer;
    [SerializeField]
    private int _visionRange = 7;
    [SerializeField]
    private GameObject _death;
    [SerializeField]
    private GameObject _zzzAnimation;
    [SerializeField]
    private GameObject _questionAnimation;
    [SerializeField]
    private MonsterEyes _eyes;

    private bool _awake = false;
    public bool Awake 
    { 
        get {return _awake; }
        set
        {
            _awake = value;
            if (_zzzAnimation != null)
                _zzzAnimation.SetActive(!_awake);
        }
    }

    protected GameObject _chaseTarget = null;
    public Animator _animator;
    private Coroutine _sleepTimer;
    private int _currAlert = 0;
    private AStarPathfinding _pathfinder;

    //audio
    public AudioClip[] sfx_awaken;
    public AudioClip[] sfx_return;
    private bool _awakened = false;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        _currPos = transform.position;
        _endPos = transform.position;
        _animator = GetComponent<Animator>();
        _animator.enabled = false;
        _pathfinder = GetComponent<AStarPathfinding>();
        RefreshAnimationState();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (_chaseTarget == null)
            return;

        if (_moving == MoveDirection.Neutral)
            SetNewTarget();
        ReachEndCheck();
    }

    protected virtual void SetNewTarget()
    {
        if (_chaseTarget == null)
            return;

        Vector3 targetPos = _chaseTarget.GetComponent<MovementController>()._currPos;
        if (DistanceCheck(targetPos) > PlayerController.Instance.Vision + _visionRange)
        {
            ReturnToStart();
            return;
        }
        Vector3 nextPos = _pathfinder.NextSquare(_currPos, targetPos, _chaseTarget.transform);  //Gives next square to move to

        if (nextPos.x > _currPos.x && _grid.IsTerrainWalkable(_currPos.x + 1, _currPos.y))
        {
            _endPos = _currPos + new Vector3(1, 0, 0);
            _moving = MoveDirection.Right;
            _rb.AddForce(500f * _moveSpeed, 0, 0);
            _renderer.flipX = true; //flips direction monster is facing
            _eyes.flipX = true;

        }
        else if (nextPos.x < _currPos.x && _grid.IsTerrainWalkable(_currPos.x - 1, _currPos.y))
        {
            _endPos = _currPos + new Vector3(-1, 0, 0);
            _moving = MoveDirection.Left;
            _rb.AddForce(-500f * _moveSpeed, 0, 0);
            _renderer.flipX = false;
            _eyes.flipX = false;
        }
        else if (nextPos.y > _currPos.y && _grid.IsTerrainWalkable(_currPos.x, _currPos.y + 1))
        {
            _moving = MoveDirection.Up;
            _endPos = _currPos + new Vector3(0, 1, 0);
            _rb.AddForce(0, 500f * _moveSpeed, 0);
        }
        else if (nextPos.y < _currPos.y && _grid.IsTerrainWalkable(_currPos.x, _currPos.y - 1))
        {
            _endPos = _currPos + new Vector3(0, -1, 0);
            _moving = MoveDirection.Down;
            _rb.AddForce(0, -500f * _moveSpeed, 0);
        }
        else
            return;
    }

    private void RefreshAnimationState()
    {
        bool halfAwake = Awake && _chaseTarget == null;
        if (_questionAnimation != null)
        {
            _questionAnimation.SetActive(halfAwake);
        }
        _animator.SetBool("Half-Asleep", halfAwake);
        if (_eyes != null)
        {
            MonsterEyes.State eyesState = MonsterEyes.State.Awake;
            if (Awake)
            {
                if (halfAwake)
                {
                    eyesState = MonsterEyes.State.Sleepy;
                }
            }
            else
            {
                eyesState = MonsterEyes.State.Asleep;
            }
            _eyes.SetState(eyesState);
        }
    }

    public virtual void ReturnToStart()
    {
        _chaseTarget = null;
        transform.position = _startPos;
        _currPos = _startPos;
        _endPos = _currPos;
        _moving = MoveDirection.Neutral;
        Awake = false;
        _animator.enabled = false;
        _renderer.color = Color.black;

        //audio
        _awakened = false;
        int i = UnityEngine.Random.Range(0, sfx_return.Length - 1);
        //GetComponent<AudioSource>().volume = 0.15f;
        GetComponent<AudioSource>().PlayOneShot(sfx_return[i]);

        RefreshAnimationState();
    }

    private int DistanceCheck(Vector3 otherPos)
    {
        int sum = 0;
        sum += (int) Mathf.Abs(_currPos.x - otherPos.x);
        sum += (int)Mathf.Abs(_currPos.y - otherPos.y);

        int sumStart = 0;
        sumStart += (int)Mathf.Abs(_startPos.x - otherPos.x);
        sumStart += (int)Mathf.Abs(_startPos.y - otherPos.y);
        return Mathf.Min(sum, sumStart);
    }

    public void Chase(GameObject target)
    {
        StopCoroutine(_sleepTimer);
        _chaseTarget = target;

        // Actually Awake
        RefreshAnimationState();

        //audio
        if (_awakened == false)
        {
            //GetComponent<AudioSource>().volume = 1;
            _awakened = true;
            int i = UnityEngine.Random.Range(0, sfx_awaken.Length - 1);
            switch (tag)
            {
                case "ghoul":
                    GetComponent<AudioSource>().PlayOneShot(sfx_awaken[i]);
                    break;
                case "chaser":
                    GetComponent<AudioSource>().PlayOneShot(sfx_awaken[i]);
                    break;
                case "charger":
                    GetComponent<AudioSource>().PlayOneShot(sfx_awaken[i]);
                    break;
                case "skully":
                    GetComponent<AudioSource>().PlayOneShot(sfx_awaken[i]);
                    break;
            }
               
        }
        
    }

    public void NotifyPlayerLeft()
    {
        _currAlert = 0;
        if (_zzzAnimation != null)
            _zzzAnimation.GetComponent<Animator>().SetInteger("alert", _currAlert);
    }

    public void WakeUp()
    {
        _currAlert++;
        if (_zzzAnimation != null)
            _zzzAnimation.GetComponent<Animator>().SetInteger("alert", _currAlert);
        if (_currAlert >= _alertDelay)
        {
            _currAlert = 0;
            if (_zzzAnimation != null)
                _zzzAnimation.GetComponent<Animator>().SetInteger("alert", _currAlert);
            Awake = true;
            _renderer.color = Color.white;
            _animator.enabled = true;
            // Show Half Awake
            if (_sleepTimer != null)
            {
                StopCoroutine(_sleepTimer);
            }
            _sleepTimer = StartCoroutine(FallAsleep());
            RefreshAnimationState();
        }
    }

    protected virtual IEnumerator FallAsleep()
    {
        yield return new WaitForSeconds(_wakeTime);
        Awake = false;
        _animator.enabled = false;
        _renderer.color = Color.black;
        RefreshAnimationState();
    }

    private void OnDestroy()
    {
        if (_death != null)
        {
            GameObject death = Instantiate(_death);
            death.transform.position = transform.position;
            death.GetComponent<Animator>().SetBool("Death", true);
        }
        if (_sleepTimer != null)
            StopCoroutine(_sleepTimer);
    }

}
