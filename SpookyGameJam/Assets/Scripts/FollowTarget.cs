﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    private bool _isTargetValid = false;

    [SerializeField] private Transform _target;

    [SerializeField] private float _shakeMagnitude = 0.7f;
    [SerializeField] private float _dampingSpeed = 1.0f;
    [SerializeField] private float _shakeDuration = 2.0f;
    private float _shakeTimeLeft = 0.0f;

    public Transform Target
    {
        get { return _target; }
        set
        {
            _target = value;
            _isTargetValid = _target != null;
        }
    }

    void Start()
    {
        _isTargetValid = _target != null;    
    }

    void Update()
    {
        if (_isTargetValid)
        {
            Vector3 desiredPosition = _target.position;
            desiredPosition.z = transform.position.z;

            if (_shakeTimeLeft > 0)
            {
                transform.localPosition = desiredPosition + Random.insideUnitSphere * _shakeMagnitude;
                _shakeTimeLeft -= Time.deltaTime * _dampingSpeed;
            }
            else
            {
                _shakeTimeLeft = 0f;
                transform.localPosition = desiredPosition;
            }
        } 
    }

    public void TriggerShake()
    {
        _shakeTimeLeft = _shakeDuration;
    }
}
