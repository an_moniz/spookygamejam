﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charger : MovementController
{
    [SerializeField]
    private SpriteRenderer _renderer;
    public bool _moveLeft = true;

    // Update is called once per frame
    void Update()
    {
        if (_moving == MoveDirection.Neutral) //have completed our movement?
        {
            SetNewTarget();
        }
        else
        {
            ReachEndCheck(); //<- this function is in MovementController
        }
    }

    private void SetMovement(Vector3 direction)
    {
        if (direction.x > 0)
        {
            _moving = MoveDirection.Right;
            _renderer.flipX = true;
        }
        else if (direction.x < 0)
        {
            _moving = MoveDirection.Left;
            _renderer.flipX = false;
        }
        else
        {
            _moving = MoveDirection.Neutral;
        }
        _endPos = _currPos + direction;
        _rb.AddForce(500f * direction.x * _moveSpeed, 0, 0);
    }

    private void SetNewTarget()
    {
        if (_moveLeft && _grid.IsTerrainWalkable(_currPos.x - 1, _currPos.y))
        {
            SetMovement(new Vector3(-1f, 0, 0));
        }
        else if (!_moveLeft && _grid.IsTerrainWalkable(_currPos.x + 1, _currPos.y))
        {
            SetMovement(new Vector3(1f, 0, 0));
        }
        else
        {
            _moveLeft = !_moveLeft;
            _renderer.flipX = !_renderer.flipX;
        }    
    }

    private void OnCollisionEnter(Collision other)
    {
        _rb.velocity = new Vector3(0, 0, 0);
        SetNewTarget();
        if (!other.gameObject.GetComponent<Chaser>() && other.gameObject.GetComponent<MonsterController>())
        {
            other.gameObject.GetComponent<MonsterController>().ReturnToStart();
        }
    }
}
