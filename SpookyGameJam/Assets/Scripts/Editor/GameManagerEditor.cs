﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    private GameManager _gameManager;
    private int _levelIndexToLoad;

    void OnEnable()
    {
        _gameManager = (GameManager)target;    
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (_gameManager.transform.childCount > 0)
        {
            _levelIndexToLoad = EditorGUILayout.IntField("Level To Load:", _levelIndexToLoad);
            if (GUILayout.Button("Load Level"))
            {
                _gameManager.LoadLevel(_levelIndexToLoad);
            }
        }
    }
}
