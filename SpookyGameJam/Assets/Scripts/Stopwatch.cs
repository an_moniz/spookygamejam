﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Stopwatch : MonoBehaviour 
{
    float timer;
    float seconds;
    float minutes;
    float hours;

    [SerializeField] TextMeshProUGUI StopwatchText;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
    }

    // Update is called once per frame
    private void Update()
    {
        StopWatchCalcul();
    }

    private void StopWatchCalcul()
    {
        timer += Time.deltaTime;
        seconds = (int) (timer % 60);
        minutes = (int) ((timer / 60) % 60);
        hours = (int) (timer / 3600);

        StopwatchText.text = hours.ToString("00:") + minutes.ToString("00:") + seconds.ToString("00");
    }
    public void Restart()
    {
        //Restarts timer upon each level start
        timer = 0;
    }

}
