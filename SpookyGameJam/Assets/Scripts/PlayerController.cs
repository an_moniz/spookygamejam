﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Create variable for sight range that can be used to adjust in-game sight range and used to by monsters to know if player can see them
public class PlayerController : MovementController
{
    private static PlayerController _instance;  //field
    public static PlayerController Instance { get { return _instance; } } //Property

    private SpriteRenderer _spriteRenderer;

    //Singleton Pattern
    private void Awake()
    {
        if (_instance != null && _instance != this)
            DestroyImmediate(gameObject);
        else
            _instance = this;

        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    //audio(footsteps)    
    AudioSource footsteps;
    private bool steps_playing = false;
    protected override void Start()
    {
        base.Start();
        SetNewVision(vision);

        footsteps = GetComponent<AudioSource>();

    }

    public delegate void StepTaken(GameObject mover);
    public static event StepTaken Stepping;

    public delegate void DeathHandler();
    public event DeathHandler DeathEvent;

    [SerializeField]
    private int vision;
    public int Vision { get; }
    [SerializeField]
    private GameObject _visionScalar;
    [SerializeField] SpriteRenderer _renderer;
    [SerializeField] Animator _animator;

    // Update is called once per frame
    void Update()
    {
        if (_moving == MoveDirection.Neutral) //have completed our movement?
        {
            //Look for new input as player is not moving
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0) {
                SetNewTarget(new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0));
            } else {
                _animator.SetBool("Walking", false);
                //audio
                footsteps.Stop();
                steps_playing = false;
            }
        }
        else
            TryQuickTurn();
        ReachEndCheck(); //<- this function is in MovementController
    }

    public void SetupForNewMap(Vector3 position)
    {
        transform.position = _endPos = _currPos = position;
        _moving = MoveDirection.Neutral;
        _animator.SetBool("Walking", false);
        _rb.velocity = Vector3.zero;
    }

    private void SetNewTarget(Vector3 direction)
    {
        _animator.SetBool("Walking", true);//enable walking animation

        if (direction.x < 0)
            _renderer.flipX = true;
        else if (direction.x > 0)
            _renderer.flipX = false;
        //horizonal movement

        //audio
        if ((_animator.GetBool("Walking") == true) && steps_playing == false) {
            footsteps.Play();
            steps_playing = true;
        } 

        if ((direction.x > 0 && _grid.IsTerrainWalkable(_currPos.x + 1, _currPos.y)) || (direction.x < 0 && _grid.IsTerrainWalkable(_currPos.x - 1, _currPos.y))) {
            _endPos = _currPos + new Vector3(direction.x, 0, 0);
            if (direction.x < 0) 
                _moving = MoveDirection.Left;
            else 
                _moving = MoveDirection.Right;
            _rb.AddForce(direction.x * 500f * _moveSpeed, 0, 0);
            //verticle movement
        } else if ((direction.y > 0 && _grid.IsTerrainWalkable(_currPos.x, _currPos.y + 1)) || (direction.y < 0 && _grid.IsTerrainWalkable(_currPos.x, _currPos.y - 1))) {
            _endPos = _currPos + new Vector3(0, direction.y, 0);
            if (direction.y < 0)
                _moving = MoveDirection.Down;
            else
                _moving = MoveDirection.Up;
            _rb.AddForce(0, direction.y * 500f * _moveSpeed, 0);
        } else 
            return;

        Stepping?.Invoke(gameObject);
    }

    public void SetNewVision(int visionSize)
    {
        vision = visionSize;
        float newScale = .25f + vision * .0675f;
        _visionScalar.transform.localScale = new Vector3(newScale, newScale, newScale);
    }

    void OnTriggerEnter(Collider other)
    {
        string[] enemies = { "Monster", "Chaser" };
        if ( ((1 << other.gameObject.layer) & LayerMask.GetMask(enemies)) != 0 )
        {
            if (!other.isTrigger)
            {
                PlayerController.Instance.DeathEvent();
                Destroy(PlayerController.Instance);
            }
            else if (other.CompareTag("VisionRange"))
            {
                Camera.main.GetComponent<FollowTarget>().TriggerShake();
            }
        }
    }

    private void TryQuickTurn()
    {
        if (_moving == MoveDirection.Right && Input.GetAxisRaw("Horizontal") == -1)
        {
            if (_endPos != _currPos)
            {
                _endPos = _currPos;
                _renderer.flipX = true;
                _moving = MoveDirection.Left;
                _rb.AddForce(-1000f * _moveSpeed, 0, 0);
            }
        }
        else if (_moving == MoveDirection.Left && Input.GetAxisRaw("Horizontal") == 1)
        {
            if (_endPos != _currPos)
            {
                _endPos = _currPos;
                _renderer.flipX = false;
                _moving = MoveDirection.Right;
                _rb.AddForce(1000f * _moveSpeed, 0, 0);
            }
        }
        else if (_moving == MoveDirection.Up && Input.GetAxisRaw("Vertical") == -1)
        {
            if (_endPos != _currPos)
            {
                _endPos = _currPos;
                _moving = MoveDirection.Down;
                _rb.AddForce(0, -1000f * _moveSpeed, 0);
            }
        }
        else if (_moving == MoveDirection.Down && Input.GetAxisRaw("Vertical") == 1)
        {
            if (_endPos != _currPos)
            {
                _endPos = _currPos;
                _moving = MoveDirection.Up;
                _rb.AddForce(0, 1000f * _moveSpeed, 0);
            }
        }
    }

}
