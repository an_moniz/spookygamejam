﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class OverlayGenerator : MonoBehaviour
{
    // Start is called before the first frame update
    public GameManager LevelOverlay;
    private TextMeshProUGUI LevelText;
    void Start()
    {
        LevelText = GetComponent<TextMeshProUGUI>(); //FULLY OPTIMIZED
    }
    public void Update()
    {
        LevelText.SetText("Level: " + (LevelOverlay.CurrentLevel + 1));
    }
}