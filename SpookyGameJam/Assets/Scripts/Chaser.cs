﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chaser : MonsterController
{
    [SerializeField]
    private BoxCollider boxCollider;
    private float timer = 0;
    private int speedMod = 0;
    protected bool _spawnedIn = false;

    protected override void Start()
    {
        base.Start();
        PlayerController.Stepping += SpawnIn;
        boxCollider.enabled = false;
    }

    private void OnDestroy()
    {
        PlayerController.Stepping -= SpawnIn;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (!_spawnedIn)
            return;
        base.Update();
        timer += Time.deltaTime;
        if (timer > 10)
        {
            timer = 0;
            speedMod = 1;
        }
    }

    public void SpawnIn(GameObject player)
    {
        _animator.enabled = true;
        _chaseTarget = player;
        StartCoroutine(AnimateSpawn());
    }

    private IEnumerator AnimateSpawn()
    {
        yield return new WaitForSeconds(1);
        boxCollider.enabled = true;
        GetComponent<Animator>().SetBool("Walking", true);
        _spawnedIn = true;
    }

    protected override void SetNewTarget()
    {
        if (speedMod > 0)
        {
            _moveSpeed *= 1 + (.2f * speedMod);
            speedMod = 0;
        }
        else if (speedMod < 0)
        {
            _moveSpeed *= .95f;
            speedMod = 0;
        }
        
        base.SetNewTarget();
    }

    protected override IEnumerator FallAsleep()
    {
        //Override Monster Controller to prevent being called
        yield return null;
    }

    public override void ReturnToStart()
    {
        //Override Monster Controller to prevent being called
    }

    private void OnCollisionEnter(Collision other)
    {
        _rb.velocity = new Vector3(0, 0, 0);
        SetNewTarget();
        if (other.gameObject.GetComponent<MonsterController>() || other.gameObject.GetComponent<Charger>())
        {
            Destroy(other.gameObject);
            speedMod = -1;
            timer = 0;
        }
    }
}