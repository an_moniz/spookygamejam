﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private MapGenerator _mapGeneratorPrefab;
    private MapGenerator _mapGenerator;
    [SerializeField] private Texture2D[] _mapLevels;
    [SerializeField] private TextAsset[] _afterLevelTexts;

    [SerializeField] private int _mainLevelCount;

    [SerializeField] private float _levelLoadTime;
    [SerializeField] private TransitionScreen _transitionScreen;

    [SerializeField] private RectTransform _startScreen;
    [SerializeField] private RectTransform _gameHUD;
    [SerializeField] private RectTransform _victoryScreen;
    [SerializeField] private RectTransform _deathScreen;
    [SerializeField] private RectTransform _creditsScreen;

    private GameStateEnum.GameState _gameState = GameStateEnum.GameState.StartScreen;
    private GameStateEnum.GameState GameState

    {
        get { return _gameState; }
        set
        {
            if (_mapGenerator)
            {
                _mapGenerator.gameObject.SetActive(value == GameStateEnum.GameState.GameScreen);
            }
            _gameState = value;
            _gameHUD.gameObject.SetActive(_gameState == GameStateEnum.GameState.GameScreen);
            _transitionScreen.gameObject.SetActive(_gameState == GameStateEnum.GameState.TransitionScreen);
            _startScreen.gameObject.SetActive(_gameState == GameStateEnum.GameState.StartScreen);
            _victoryScreen.gameObject.SetActive(_gameState == GameStateEnum.GameState.VictoryScreen);
            _deathScreen.gameObject.SetActive(_gameState == GameStateEnum.GameState.FailureScreen);
            _creditsScreen.gameObject.SetActive(_gameState == GameStateEnum.GameState.CreditsScreen);

            //audio
            if ( _gameState == GameStateEnum.GameState.StartScreen)
            {
                bgMusic.Stop();
            }
            else if ( _gameState == GameStateEnum.GameState.GameScreen && firstLaunch)
            {
                musicStartLooping = music[0].length;
                bgMusic.volume = 0.10f;
                bgMusic.PlayOneShot(music[0]);
                firstLaunch = false;
                playTheMusic = true;
                introElapsed = 0;
            }
            else if (_gameState == GameStateEnum.GameState.FailureScreen)
            {
                bgMusic.Stop();
                firstLaunch = true;
                playTheMusic = false;
            }
        }
    }

    private Stopwatch _stopWatch;

    private Camera _mainCamera;

    private MapGrid _currentMap;
    public MapGrid CurrentMap { get {return _currentMap; } }

    private int _levelIndex = 0;
    public int CurrentLevel
    {
        get { return _levelIndex; }
        set { _levelIndex = value; }
    }

    public int LevelCount
    {
        get { return _mapLevels.Length; }
    }

    //audio
    public AudioClip[] music;
    private AudioSource bgMusic;
    private float musicStartLooping = 99;
    private float introElapsed;
    private bool playTheMusic = true;
    private bool firstLaunch = true;

    void Awake()
    {
        _mainCamera = Camera.main;
        _mapGenerator = Instantiate<MapGenerator>(_mapGeneratorPrefab);
        _mapGenerator.transform.parent = transform;

        _transitionScreen.gameObject.SetActive(false);
        _startScreen.gameObject.SetActive(false);
        _gameHUD.gameObject.SetActive(false);

        bgMusic = GetComponent<AudioSource>();

        _stopWatch = FindObjectOfType<Stopwatch>();
    }

    void Start()
    {
        GoToStartScreen();
    }

    void Update()
    {
        if (GameState == GameStateEnum.GameState.StartScreen)
        {
            if (Input.anyKeyDown)
            {
                StartGame();
            }
        }
        else if (GameState == GameStateEnum.GameState.VictoryScreen)
        {
            if (Input.anyKeyDown)
            {
                GoToStartScreen();
            }
        }
        else if (GameState == GameStateEnum.GameState.FailureScreen)
        {
            if (Input.anyKeyDown)
            {
                GameState = GameStateEnum.GameState.GameScreen;
                LoadLevel(CurrentLevel);
            }
        }
        else if (GameState == GameStateEnum.GameState.CreditsScreen)
        {
            if (Input.anyKeyDown)
            {
                GameState = GameStateEnum.GameState.GameScreen;
                LoadLevel(CurrentLevel + 1);
            }
        }

        //audio
        if (playTheMusic)
        {
            introElapsed += Time.deltaTime;
            if (introElapsed >= musicStartLooping)
            {
                bgMusic.volume = 0.10f;
                bgMusic.clip = music[1];
                bgMusic.loop = true;
                bgMusic.Play();
                playTheMusic = false;
            }
        }

    }

    public void LoadLevel( int levelIndex )
    {
        if ( levelIndex < _mapLevels.Length )
        {
            if (_stopWatch != null)
                _stopWatch.Restart();
            FollowTarget followTarget = _mainCamera.GetComponent<FollowTarget>();
            if (followTarget != null)
            {
                followTarget.Target = null;
            }
            _levelIndex = levelIndex;
            _currentMap = _mapGenerator.GenerateMap(_mapLevels[levelIndex]);
            if (PlayerController.Instance != null)
            {
                followTarget.Target = PlayerController.Instance.transform;
                PlayerController.Instance.Stepped += OnPlayerStep;
                PlayerController.Instance.DeathEvent += OnPlayerDeath;
            }
        }
    }

    private void OnPlayerStep()
    {
        if (_gameState == GameStateEnum.GameState.GameScreen && _currentMap.VictoryNode != null && _currentMap.NodeFromWorldPoint(PlayerController.Instance.transform.position) == _currentMap.VictoryNode )
        {
            PlayerController.Instance.Stepped -= OnPlayerStep;
            LoadNextLevel();
        }
    }

    private void OnPlayerDeath()
    {
        GameState = GameStateEnum.GameState.FailureScreen;
    }

    private void LoadNextLevel()
    {
        GameState = GameStateEnum.GameState.TransitionScreen;
        if (CurrentLevel < _afterLevelTexts.Length)
        {
            _transitionScreen.SetText(_afterLevelTexts[CurrentLevel].text);
        }

        int NextLevel = CurrentLevel + 1;
        if (NextLevel == _mainLevelCount)
        {
            StartCoroutine("ShowCredits");
        }
        else
        {
            if (NextLevel < LevelCount)
            {
                StartCoroutine("LevelLoadCoroutine");
            }
            else
            {
                StartCoroutine("LoadVictoryScreenCoroutine");
            }
        }
    }

    private IEnumerator ShowCredits()
    {
        yield return new WaitForSeconds(_levelLoadTime);
        GameState = GameStateEnum.GameState.CreditsScreen;
    }

    private IEnumerator LevelLoadCoroutine()
    {
        yield return new WaitForSeconds(_levelLoadTime);
        GameState = GameStateEnum.GameState.GameScreen;
        LoadLevel(CurrentLevel + 1);
    }

    private IEnumerator LoadVictoryScreenCoroutine()
    {
        yield return new WaitForSeconds(_levelLoadTime);
        GameState = GameStateEnum.GameState.VictoryScreen;

    }

    public void GoToStartScreen()
    {
        CleanGame();
        GameState = GameStateEnum.GameState.StartScreen;
    }

    private void CleanGame()
    {
        _mapGenerator.CleanMap();
        _currentMap = null;

        if (PlayerController.Instance)
            PlayerController.Instance.gameObject.SetActive(false);
    }

    public void StartGame()
    {
        GameState = GameStateEnum.GameState.GameScreen;
        LoadLevel(0);

        if (PlayerController.Instance)
            PlayerController.Instance.gameObject.SetActive(true);        
    }
}
