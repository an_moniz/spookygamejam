﻿using UnityEngine;
using System.Collections;

public class Node : System.IEquatable<Node>
{
	public bool walkable;
	public Vector3 worldPosition;
	public int gridX;
	public int gridY;

	public int gCost;
	public int hCost;
	public Node parent;

    public Node(bool _walkable, Vector3 worldPosition, int _gridX, int _gridY)
    {
        walkable = _walkable;
        worldPosition = worldPosition;
        gridX = _gridX;
        gridY = _gridY;
    }

    public bool Equals(Node other)
    {
        if (ReferenceEquals(other, null))
        {
            return false;
        }
        if (ReferenceEquals(this, other))
        {
            return true;
        }
        return gridX == other.gridX && gridY == other.gridY;
    }

    public override bool Equals(object obj)
    {
        return Equals(obj as Node);
    }
    public static bool operator ==(Node a, Node b)
    {
        if (ReferenceEquals(a, b))
            return true;
        if (ReferenceEquals(a, null))
            return false;
        if (ReferenceEquals(b, null))
            return false;
        return a.Equals(b);
    }
    public static bool operator !=(Node a, Node b)
    {
        return !(a == b);
    }

	public int fCost
	{
		get
		{
			return gCost + hCost;
		}
	}
}