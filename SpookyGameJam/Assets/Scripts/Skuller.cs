﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skuller : MonoBehaviour
{
    private bool _spawnedIn = false;
    private Animator _animator;
    private BoxCollider boxCollider;

    public GameObject _characterInRange;

    // Start is called before the first frame update
    void Start()
    {
        PlayerController.Stepping += DetectMovement;
        _animator = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider>();
    }

    private void OnDestroy()
    {
        PlayerController.Stepping -= DetectMovement;
    }

    public void DetectMovement(GameObject mover)
    {
        if (_characterInRange == null || _characterInRange != mover || _spawnedIn)
            return;
        SpawnIn();
        GetComponent<AudioSource>().Play();
    }

    public void SpawnIn()
    {
        _spawnedIn = true;
        _animator.enabled = true;
        StartCoroutine(AnimateSpawn());
    }

    private IEnumerator AnimateSpawn()
    {
        yield return new WaitForSeconds(.3f);
        boxCollider.enabled = true;
        yield return new WaitForSeconds(1.33f);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.GetComponent<PlayerController>())
            _characterInRange = collision.gameObject;
    }

    private void OnTriggerExit(Collider collision)
    {
        _characterInRange = null;
    }
}
