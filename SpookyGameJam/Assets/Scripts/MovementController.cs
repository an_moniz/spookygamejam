﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField]
    protected float _moveSpeed = 1.4f;
    public Vector3 _currPos = new Vector3(0, 0, 0);
    protected Vector3 _endPos = new Vector3(0, 0, 0);
    protected Vector3 _startPos;

    protected GameManager _gameManager;
    protected MapGrid _grid { get => _gameManager.CurrentMap; }
    protected Rigidbody _rb;

    protected enum MoveDirection
    {
        Up,
        Down,
        Left,
        Right,
        Neutral
    }
    protected MoveDirection _moving = MoveDirection.Neutral;

    public delegate void StepHandler();
    public event StepHandler Stepped;
    protected virtual void Start()
    {
        _currPos = transform.position;
        _endPos = transform.position;
        _startPos = transform.position;
        _gameManager = FindObjectOfType<GameManager>();
        _rb = GetComponent<Rigidbody>();
    }

    protected void ReachEndCheck()
    {
        if (_moving == MoveDirection.Right && transform.position.x > _endPos.x)
        {
            _rb.AddForce(-500f * _moveSpeed, 0, 0);
            _currPos = _endPos;
            _moving = MoveDirection.Neutral;
            Stepped?.Invoke();
        }
        else if (_moving == MoveDirection.Left && transform.position.x < _endPos.x)
        {
            _rb.AddForce(500f * _moveSpeed, 0, 0);
            _currPos = _endPos;
            _moving = MoveDirection.Neutral;
            Stepped?.Invoke();
        }
        else if (_moving == MoveDirection.Up && transform.position.y > _endPos.y)
        {
            _rb.AddForce(0, -500f * _moveSpeed, 0);
            _currPos = _endPos;
            _moving = MoveDirection.Neutral;
            Stepped?.Invoke();
        }
        else if (_moving == MoveDirection.Down && transform.position.y < _endPos.y)
        {
            _rb.AddForce(0, 500f * _moveSpeed, 0);
            _currPos = _endPos;
            _moving = MoveDirection.Neutral;
            Stepped?.Invoke();
        }
    }
}
