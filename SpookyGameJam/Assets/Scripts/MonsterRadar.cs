﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterRadar : MonoBehaviour
{
    public SpriteRenderer _sleepy;
    public GameObject _characterInRange;
    private MonsterController _movement;

    // Start is called before the first frame update
    void Start()
    {
        PlayerController.Stepping += DetectMovement;
        _movement = GetComponentInParent<MonsterController>();
    }

    private void OnDestroy()
    {
        PlayerController.Stepping -= DetectMovement;
    }

    public void DetectMovement(GameObject mover)
    {
        if (_characterInRange == null || _characterInRange != mover)
            return;

        if (_movement.Awake)
            _movement.Chase(_characterInRange);
        else
            _movement.WakeUp();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            _characterInRange = collision.gameObject;
            if (_sleepy != null)
                _sleepy.sortingOrder = 100;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (!collision.GetComponent<PlayerController>())
            return;
        if (_characterInRange != null)
        {
            _movement.NotifyPlayerLeft();
        }
        _characterInRange = null;
        if (_sleepy != null)
            _sleepy.sortingOrder = 2;
    }
}
